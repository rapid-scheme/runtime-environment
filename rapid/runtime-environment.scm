;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Runtime environments

(define-record-type <runtime-environment>
  (%make-runtime-environment library-vicinities feature-identifiers libraries)
  runtime-environment?
  (library-vicinities runtime-environment-library-vicinities)
  (feature-identifiers runtime-environment-feature-identifiers)
  (libraries runtime-environment-libraries runtime-environment-set-libraries!))

;;; TODO: The runtime environment should possibly include (scheme base), ...
(define (make-runtime-environment library-vicinities feature-identifiers)
  (%make-runtime-environment library-vicinities
			     (list->set symbol-comparator feature-identifiers)
			     (mapping library-name-comparator
				      '(rapid primitive) primitive-library)))

(define (with-runtime-environment runtime-environment thunk)
  (parameterize ((feature-identifier-predicate
		  (lambda (symbol)
		    (set-contains? (runtime-environment-feature-identifiers
				    runtime-environment)
				   symbol)))
		 (library-importability-predicate
		  (make-library-importability-predicate runtime-environment))
		 (library-exports-accessor
		  (make-library-exports-accessor runtime-environment))
		 (library-dependencies-accessor
		  (make-library-dependencies-accessor runtime-environment))
		 (library-environment-accessor
		  (make-library-environment-accessor runtime-environment))
		 (library-store-accessor
		  (make-library-store-accessor runtime-environment))
		 (current-library-vicinities
		  (runtime-environment-library-vicinities
		   runtime-environment)))
    (thunk)))

(define (library-ref runtime-environment context library-name)
  (receive (libraries library)
      (mapping-intern! (runtime-environment-libraries runtime-environment)
		       library-name
		       (lambda ()
			 (read-library context library-name)))
    (runtime-environment-set-libraries! runtime-environment
					libraries)
    library))

(define (make-library-importability-predicate runtime-environment)
  (lambda (context library-name)
    (library-read? (library-ref runtime-environment context library-name))))

(define (make-library-exports-accessor runtime-environment)
  (lambda (context library-name)
    (let ((library (library-ref runtime-environment context library-name)))
      (cond
       ((%library-exports library)
	=> (lambda (exports)
	     (if (eq? #t exports)
		 (raise-syntax-error context
				     "circular reference to library ‘~a’"
				     library-name)
		 exports)))
       ((%library-declarations library)
	=> (lambda (declarations)
	     (library-set-exports! library #t)
	     (receive (exports imports dependencies body)
		 (analyze-library library-name declarations)
	       (library-set-exports! library exports)
	       (library-set-imports! library imports)
	       (library-set-dependencies! library dependencies)
	       (library-set-body! library body)
	       exports)))
       (else
	(raise-syntax-error context "couldn't find library ‘~a’" library-name))))))

(define (make-library-dependencies-accessor runtime-environment)
  (lambda (library-name)
    (let ((library (library-ref runtime-environment #f library-name)))
      (%library-dependencies library))))

(define (make-library-environment-accessor runtime-environment)
  (lambda (library-name)
    (let ((library (library-ref runtime-environment #f library-name)))
      (cond
       ((%library-environment library))
       (else
	(receive (environment definitions)
	    (expand-library library-name
			    (%library-exports library)
			    (%library-imports library)
			    (%library-body library))
	  (library-set-environment! library environment)
	  (library-set-definitions! library definitions)
	  (library-set-imports! library #t)
	  (library-set-body! library #t)))))))

(define (make-library-store-accessor runtime-environment)
  (lambda (library-name)
    (let ((library (library-ref runtime-environment #f library-name)))
      (cond
       ((%library-store library))
       (else
	(let ((dependencies (%library-dependencies library))
	       (definitions (%library-definitions library)))
	   ;; CATCH AND RETHROW?
	   (let ((store
		  (evaluate-library dependencies
				    definitions)))
	     (library-set-store! library store)
	     (library-set-definitions! library #t)
	     store)))))))

(define-record-type <library>
  (%make-library declarations exports dependencies environment store)
  library?
  (declarations %library-declarations library-set-declarations!)
  (exports %library-exports library-set-exports!)
  (imports %library-imports library-set-imports!)
  (dependencies %library-dependencies library-set-dependencies!)
  (body %library-body library-set-body!)
  (environment %library-environment library-set-environment!)
  (definitions %library-definitions library-set-definitions!)
  (store %library-store library-set-store!))

(define (make-library text)
  (%make-library text #f #f #f #f))

(define (library-read? library)
  (and (%library-declarations library) #t))

(define (read-library context library-name)
  (let loop ((library-vicinities (current-library-vicinities)))
    (if (null? library-vicinities)
	(make-library #f)
	(let ((filename (library-filename (car library-vicinities) library-name)))
	  (if (file-exists? filename)
	      (let ((definitions (read-file #f filename #f)))
		(let definitions-loop ((definitions definitions))
		  (if (null? definitions)
		      (loop (cdr library-vicinities))
		      (syntax-match (car definitions)
			((define-library ,(unwrap-library-name -> name)
			   ,declaration* ...)
			 (if (and name
				  (equal? name library-name))
			     (make-library declaration*)
			     (definitions-loop (cdr definitions))))
			(,definition
			  (raise-syntax-error definition
					      "invalid library definition")
			  (definitions-loop (cdr definitions)))))))
	      (loop (cdr library-vicinities)))))))

(define (library-filename vicinity library-name)
  (let ((pathname (library-element->pathname (car library-name))))
    (if (null? (cdr library-name))
	(in-vicinity vicinity (string-append pathname ".sld"))
	(library-filename (sub-vicinity vicinity pathname)
			  (cdr library-name)))))

(define (library-element->pathname library-element)
  (if (number? library-element)
      (number->string library-element)
      (symbol->string library-element)))

;;;; Parameter objects

;;> \section{Parameter objects}


;;; NEEDED? XXX
(define current-library-vicinities (make-parameter '()))

;;;; Values

(define primitive-library
  (%make-library #t
		 rapid-primitive-exports
		 (set library-name-comparator)
		 rapid-primitive-environment
		 rapid-primitive-store))
