;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid runtime-environment-test)
  (export run-tests)
  (import (scheme base)
	  (scheme file)
	  (scheme write)
	  (rapid test)
	  (rapid mapping)
	  (rapid vicinity)
	  (rapid analyze-library)
	  (rapid runtime-environment))
  (begin
    (define (run-tests)
    
      (test-begin "Runtime environments")

      (write-library!)

      (with-runtime-environment
	  (make-runtime-environment (list (user-vicinity))
				    '())
	(lambda ()

	  (test-assert "library-importable?: true"
	    (library-importable? #f '(library)))

	  (test-assert "library importable?: false"
	    (not (library-importable? #f '(foo))))

	  (delete-library!)
	  
	  (test-assert "library-importable?: in cache"
	    (library-importable? #f '(library)))

	  (test-error "library-exports: not found"
	    (library-exports #f '(foo)))

	  (test-equal "library-exports: found"
	    'bar
	    (qualified-identifier-symbol
	     (mapping-ref (library-exports #f '(library)) 'bar)))

	  (test-equal "library-exports: primitives"
	    'begin
	    (qualified-identifier-symbol
	     (mapping-ref (library-exports #f '(rapid primitive)) 'begin)))))
      
      (test-end))


    (define data
      '((define-library (library)
	  (export bar))))
    (define file-name (in-vicinity (user-vicinity) "library.sld"))
    (define (delete-library!)
      (delete-file file-name))
    (define (write-library!)
      (unless (file-exists? file-name)
	(with-output-to-file file-name
	  (lambda ()
	    (for-each write data)))))))
