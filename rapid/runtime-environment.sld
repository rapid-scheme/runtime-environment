;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Runtime environments into which libraries can be loaded.

(define-library (rapid runtime-environment)
  (export current-library-vicinities
	  make-runtime-environment
	  runtime-environment?
	  with-runtime-environment)
  (import (scheme base)
	  (scheme file)
	  (rapid assume)
	  (rapid receive)
	  (rapid list)
	  (rapid comparator)
	  (rapid set)
	  (rapid mapping)
	  (rapid syntax)
	  (rapid read)
	  (rapid vicinity)
	  (rapid analyze-library)
	  (rapid expand-library)
	  (rapid evaluate-library)
	  (rapid primitives))
  (include "runtime-environment.scm"))

;; Local Variables:
;; eval: (put 'with-runtime-environment 'scheme-indent-function 1)
;; eval: (font-lock-add-keywords 'scheme-mode
;;                               '(("(\\(with-runtime-environment\\)\\>" 1 font-lock-keyword-face)))
;; End:
